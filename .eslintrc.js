'use strict';

module.exports = {
	root: true,
	extends: [
		'airbnb',
		'plugin:prettier/recommended',
		'prettier',
		'plugin:@typescript-eslint/recommended',
	],
	plugins: ['jest', 'prettier', '@typescript-eslint'],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 2021,
		ecmaFeatures: {
			jsx: true,
		},
		sourceType: 'script',
	},
	env: {
		'jest/globals': true,
		node: true,
		es6: true,
	},
	rules: {
		'arrow-parens': ['off'],
		'func-names': ['off'],
		'global-require': ['off'],
		'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'],
		'no-tabs': ['off'],
		'prefer-destructuring': ['off'],
		strict: ['error', 'global'],

		'import/prefer-default-export': ['off'],
		'import/extensions': ['off'],
		'import/no-unresolved': ['off'],

		'prettier/prettier': ['error'],

		indent: ['off'],

		'@typescript-eslint/ban-ts-comment': ['off'],
	},
	overrides: [
		{
			files: ['src/**/*'],
			parserOptions: {
				sourceType: 'module',
			},
		},
	],
};
