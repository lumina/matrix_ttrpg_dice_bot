import { nodeCrypto, dice } from 'random-js';

export type DiceRolls = {
	size: number;
	result: number[];
};

export class SimpleDiceError extends Error {}

export function diceRegex() {
	return /^(\d+)?d(\d+)$/;
}

export default function diceRoll(roll: string): DiceRolls {
	const parts = diceRegex().exec(roll);
	if (parts === null) {
		throw new SimpleDiceError(
			`Expected simple dice roll like 4d6, received ${roll}`,
		);
	}
	const count = +((parts[1] as string) ?? 1);
	const size = +(parts[2] as string);
	if (count > 100000) {
		throw new SimpleDiceError(
			`Count must be less than 100000, received ${count}`,
		);
	}
	if (count * size > 100000000000000) {
		throw new SimpleDiceError(
			`Count * size must be less than 100000000000000; received count: ${count}, size: ${size}, product: ${
				count * size
			}`,
		);
	}

	const result = dice(size, count)(nodeCrypto);
	return {
		size,
		result,
	};
}
