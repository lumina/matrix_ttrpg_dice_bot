import diceRoll, { DiceRolls, SimpleDiceError } from './diceRoll';
import roller, { drop, handleComplexRoll, keep } from './roller';

jest.mock('./diceRoll', () => {
	return {
		__esModule: true,
		default: jest.fn(),
		SimpleDiceError: jest.requireActual('./diceRoll').SimpleDiceError,
	};
});

function fakeStrictRoll(roll: string, result: DiceRolls) {
	return (r: string): DiceRolls => {
		expect(r).toBe(roll);
		return result;
	};
}

function setRolls(rolls: [roll: string, result: DiceRolls][]) {
	rolls.forEach(([roll, result]) => {
		// @ts-ignore
		diceRoll.mockImplementationOnce(fakeStrictRoll(roll, result));
	});
}

describe('roller', () => {
	it('returns roller results', () => {
		setRolls([['1d20', { result: [11], size: 20 }]]);
		expect(roller('1d20+6').value).toStrictEqual(17);
		setRolls([['d8', { result: [4], size: 8 }]]);
		expect(roller('d8+4').value).toStrictEqual(8);
		setRolls([
			['4d6', { result: [2, 5, 1, 3], size: 6 }],
			['d20', { result: [8], size: 20 }],
			['2d8', { result: [3, 5], size: 8 }],
		]);
		expect(roller('4d6 + 4* d20/20-     2d8').value).toBe(-2);
		setRolls([
			['2d6', { result: [5, 6], size: 6 }],
			['1d9', { result: [2], size: 9 }],
			['2d8', { result: [4, 1], size: 8 }],
		]);
		expect(roller('2d6 - 1d9 + 1 / 2d8').value).toBe(2);
		setRolls([
			['2d6', { result: [5, 6], size: 6 }],
			['1d9', { result: [2], size: 9 }],
		]);
		expect(() => roller('2d6 - 1d9 + 1d / 2d8')).toThrow(
			'Error: After "2d6 - 1d9 +" got unexpected "1d"',
		);
		setRolls([
			['4d6', { result: [2, 3, 1, 3], size: 6 }],
			['1d9', { result: [2], size: 9 }],
		]);
		expect(roller('4d6d1 + 1d9').value).toBe(10);
		setRolls([['4d20', { result: [12, 12, 8, 8], size: 20 }]]);
		expect(roller('4d20d2').value).toBe(24);
		setRolls([
			['3d6', { result: [1, 1, 5], size: 6 }],
			['3d8', { result: [2, 5, 6], size: 8 }],
		]);
		expect(roller('3d6dl1 * 3d8kh1').value).toBe(36);
	});
	it('correctly rounds', () => {
		setRolls([
			['1d20', { result: [18], size: 20 }],
			['1d20', { result: [7], size: 20 }],
		]);
		expect(roller('1d20 / 1d20').value).toBe(2);
		setRolls([
			['1d20', { result: [18], size: 20 }],
			['d20', { result: [7], size: 20 }],
		]);
		expect(roller('1d20 /u d20').value).toBe(3);
		setRolls([
			['1d20', { result: [18], size: 20 }],
			['1d20', { result: [7], size: 20 }],
		]);
		expect(roller('1d20 /d 1d20').value).toBe(2);
	});
	it('returns rolling history', () => {
		setRolls([['1d20', { result: [11], size: 20 }]]);
		expect(roller('1d20+6')).toStrictEqual({
			value: 17,
			rollHistory: [
				{ type: null, rolls: ['11/20'] },
				{ type: 'Fixed', rolls: ['6'] },
			],
		});
		setRolls([
			['4d6', { result: [2, 5, 1, 3], size: 6 }],
			['1d20', { result: [8], size: 20 }],
			['2d8', { result: [3, 5], size: 8 }],
		]);
		expect(roller('4d6 + 4*1d20  /20-     2d8')).toStrictEqual({
			value: -2,
			rollHistory: [
				{ type: null, rolls: ['2/6', '5/6', '1/6', '3/6'] },
				{ type: 'Fixed', rolls: ['4'] },
				{ type: null, rolls: ['8/20'] },
				{ type: 'Fixed', rolls: ['20'] },
				{ type: null, rolls: ['3/8', '5/8'] },
			],
		});
		setRolls([
			['4d6', { result: [6, 6, 1, 6], size: 6 }],
			['1d20', { result: [1], size: 20 }],
			['1d20', { result: [20], size: 20 }],
			['2d8', { result: [1, 1], size: 8 }],
		]);
		expect(roller('4d6d1 + 2*1d20*1d20  /20-     2d8')).toStrictEqual({
			value: 18,
			rollHistory: [
				{ type: 'Max', rolls: ['6/6', '6/6', '1/6', '6/6'] },
				{ type: 'Fixed', rolls: ['2'] },
				{ type: 'Nat 1', rolls: ['1/20'] },
				{ type: 'Nat 20', rolls: ['20/20'] },
				{ type: 'Fixed', rolls: ['20'] },
				{ type: 'Min', rolls: ['1/8', '1/8'] },
			],
		});
	});
	it('prints nice errors', () => {
		setRolls([['1d20', { result: [1], size: 20 }]]);
		// @ts-ignore
		diceRoll.mockImplementationOnce(() => {
			throw new SimpleDiceError(
				'Count must be less than 100000, received 100001',
			);
		});
		// eslint-disable-next-line consistent-return
		expect(() => roller('1d20 + 100001d10d1')).toThrow(
			'Error in "100001d10d1". Count must be less than 100000, received 100001',
		);
	});
});

describe('handleComplexRoll', () => {
	it('correctly parses and return', () => {
		setRolls([['5d20', { result: [2, 6, 10, 12, 18], size: 20 }]]);
		expect(handleComplexRoll('5d20dh2')).toStrictEqual({
			value: 18,
			attribute: null,
			history: ['2/20', '6/20', '10/20', '12/20', '18/20'],
		});
	});
	it('correctly parses and return', () => {
		setRolls([['5d20', { result: [2, 6, 10, 12, 18], size: 20 }]]);
		expect(handleComplexRoll('5d20k2')).toStrictEqual({
			value: 30,
			attribute: null,
			history: ['2/20', '6/20', '10/20', '12/20', '18/20'],
		});
	});
	it('correctly parses and returns for implicit count', () => {
		setRolls([['d20', { result: [2], size: 20 }]]);
		expect(handleComplexRoll('d20k1')).toStrictEqual({
			value: 2,
			attribute: null,
			history: ['2/20'],
		});
	});
});

describe('drop', () => {
	it('drops the lowest', () => {
		expect(drop(2, true, [4, 6, 2, 8])).toStrictEqual([6, 8]);
		expect(drop(3, true, [4, 6, 2, 8])).toStrictEqual([8]);
	});
	it('drops the highest', () => {
		expect(drop(2, false, [4, 6, 2, 8])).toStrictEqual([2, 4]);
		expect(drop(1, false, [2, 8])).toStrictEqual([2]);
	});
});

describe('keep', () => {
	it('keeps the highest', () => {
		expect(keep(2, true, [4, 6, 2, 8])).toStrictEqual([6, 8]);
		expect(keep(3, true, [4, 6, 2, 8])).toStrictEqual([4, 6, 8]);
	});
	it('keeps the lowest', () => {
		expect(keep(2, false, [4, 6, 2, 8])).toStrictEqual([2, 4]);
		expect(keep(1, false, [2, 8])).toStrictEqual([2]);
	});
});
