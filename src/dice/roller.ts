import DiceError from './DiceError';
import diceRoll, { SimpleDiceError } from './diceRoll';

export type RollAttribute =
	| `Nat ${number}`
	| 'Nat 1'
	| 'Max'
	| 'Min'
	| 'Fixed'
	| null;
type RollResult = {
	value: number;
	attribute: RollAttribute;
	history: string[];
};

type Operation = (a: number, b: number) => number;

const ops: { [operator: string]: Operation } = {
	'+': (a, b) => a + b,
	'-': (a, b) => a - b,
	'*': (a, b) => a * b,
	'/': (a, b) => a / b,
};
function opSplitter() {
	return /([+\-*/][ud]?)/;
}
function isOp(s: string) {
	return typeof ops[s.slice(0, 1)] === 'function';
}
function parseOp(s: string): Operation {
	const operation = ops[s.slice(0, 1)];
	if (operation == null) {
		throw new Error(`Unrecognized operator: "${s}"`);
	}
	return s.slice(1) === 'u'
		? (a, b) => Math.ceil(operation(a, b))
		: (a, b) => Math.floor(operation(a, b));
}
function isSimpleNum(s: string) {
	return /^\d+$/.test(s);
}

function descriptionError(index: number, elements: string[]) {
	return `Error: After "${elements
		.slice(0, index)
		.join(' ')}" got unexpected "${elements[index]}"`;
}

class RollerError extends DiceError {
	constructor(index: number, elements: string[]) {
		super(descriptionError(index, elements));
	}
}

type FilterFunction = (dice: number[]) => number[];

export function drop(
	number: number,
	lowest: boolean,
	dice: number[],
): number[] {
	const ordered = dice.slice().sort((a, b) => a - b);
	if (lowest) {
		return ordered.slice(number);
	}
	return ordered.slice(0, -number);
}

export function keep(
	number: number,
	highest: boolean,
	dice: number[],
): number[] {
	return drop(dice.length - number, highest, dice);
}

function isComplexRoll(s: string) {
	return /^\d*d\d+((d|k)(h|l)?\d+)?/.test(s);
}

export function handleComplexRoll(roll: string): RollResult | null {
	if (!isComplexRoll(roll)) {
		return null;
	}
	const match = /(^\d*d\d+)(.*)/.exec(roll) as RegExpExecArray;
	const basicRoll = match[1] as string;
	const rest = match[2] as string;
	const { result: rolls, size: diceSize } = diceRoll(basicRoll);
	const filters: FilterFunction[] = [];
	if (rest) {
		const elements = rest.match(/([a-z]+)/g) ?? [];
		if (
			elements.includes('d') ||
			elements.includes('dl') ||
			elements.includes('dh')
		) {
			const numResult = /[^a-z]?(dl?h?)(\d+)/.exec(rest);
			if (numResult == null) {
				throw new Error(`malformed dice roll ${roll}`);
			}
			const mod = numResult[1];
			const num = +(numResult[2] as string);
			filters.push(dice => drop(num, mod !== 'dh', dice));
		}
		if (
			elements.includes('k') ||
			elements.includes('kl') ||
			elements.includes('kh')
		) {
			const numResult = /[^a-z]?(kl?h?)(\d+)/.exec(rest);
			if (numResult == null) {
				throw new Error(`malformed dice roll ${roll}`);
			}
			const mod = numResult[1];
			const num = +(numResult[2] as string);
			filters.push(dice => keep(num, mod !== 'kl', dice));
		}
	}
	const finalRolls = filters.reduce((prev: number[], filter) => {
		return filter(prev);
	}, rolls);
	const total = finalRolls.reduce((s, d) => {
		return s + d;
	}, 0);
	let attribute = null;
	if (finalRolls.every(value => value === diceSize)) {
		attribute = finalRolls.length === 1 ? `Nat ${diceSize}` : 'Max';
	} else if (finalRolls.every(value => value === 1)) {
		attribute = finalRolls.length === 1 ? 'Nat 1' : 'Min';
	}
	return {
		value: total,
		attribute: attribute as RollAttribute,
		history: rolls.map(value => `${value}/${diceSize}`),
	};
}

function isValidNumber(s: string) {
	return isSimpleNum(s) || isComplexRoll(s);
}

function toNumber(s: string): RollResult | null {
	if (isSimpleNum(s)) {
		return { value: +s, attribute: 'Fixed', history: [s] };
	}
	return handleComplexRoll(s);
}

export function validateRollString(rollstring: string) {
	const result = rollstring
		.toLowerCase()
		.split(opSplitter())
		.map(s => s.trim())
		.reduce<string | { lastOperator: boolean; lastOperand: boolean }>(
			(state, element, index, elements) => {
				if (typeof state === 'string') {
					return state;
				}
				if (isOp(element)) {
					if (state.lastOperator || state.lastOperand === false) {
						return descriptionError(index, elements);
					}
					return {
						lastOperand: true,
						lastOperator: true,
					};
				}
				if (!isValidNumber(element) || state.lastOperator === false) {
					return descriptionError(index, elements);
				}
				return {
					lastOperand: true,
					lastOperator: false,
				};
			},
			{
				lastOperand: false,
				lastOperator: true,
			},
		);
	if (typeof result === 'string') {
		return result;
	}
	return undefined;
}

export type RollerResult = {
	value: number;
	rollHistory: { type: RollAttribute; rolls: string[] }[];
};

function roller(rollstring: string) {
	const rollHistory: { type: RollAttribute; rolls: string[] }[] = [];
	const finalTotal = rollstring
		.toLowerCase()
		.split(opSplitter())
		.map(s => s.trim())
		.reduce<{ lastOperator: Operation | null; lastOperand: number | null }>(
			(state, element, index, elements) => {
				if (isOp(element)) {
					if (state.lastOperator) {
						throw new RollerError(index, elements);
					}
					return { ...state, lastOperator: parseOp(element) };
				}
				const rollResult = (() => {
					try {
						return toNumber(element);
					} catch (error) {
						if (error instanceof SimpleDiceError) {
							throw new DiceError(`Error in "${element}". ${error.message}`);
						}
						throw error;
					}
				})();
				if (rollResult === null) {
					throw new RollerError(index, elements);
				}

				const operand = rollResult.value;
				rollHistory.push({
					type: rollResult.attribute,
					rolls: rollResult.history,
				});
				if (state.lastOperand == null) {
					return {
						lastOperand: operand,
						lastOperator: null,
					};
				}
				if (state.lastOperator == null) {
					throw new RollerError(index, elements);
				}
				return {
					lastOperand: state.lastOperator(state.lastOperand, operand),
					lastOperator: null,
				};
			},
			{
				lastOperand: null,
				lastOperator: null,
			},
		).lastOperand;
	if (finalTotal === null) {
		throw new DiceError('Null roller result');
	}
	return {
		value: finalTotal,
		rollHistory,
	};
}

export default roller;
