import { dice } from 'random-js';
import diceRoll from './diceRoll';

jest.mock('random-js');

function setRandom(
	expectedCount: number,
	expectedSides: number,
	result: number[],
) {
	// @ts-ignore
	dice.mockImplementationOnce((sides: number, count: number) => {
		return () => {
			if (sides !== expectedSides || count !== expectedCount) {
				throw new Error(`Received unexpected args for dice:
Expected: ${expectedCount}, ${expectedSides}
Received: ${count}, ${sides}
				`);
			}
			return result;
		};
	});
}

describe('diceRoll', () => {
	it.each([
		// roll, count, sides, fake rolls
		{ r: '1d6', c: 1, s: 6, f: [4] },
		{ r: '4d20', c: 4, s: 20, f: [4, 10, 14, 20] },
		{ r: 'd8', c: 1, s: 8, f: [7] },
		{ r: '6d5', c: 6, s: 5, f: [3, 4, 1, 5, 3, 2] },
	])('result for $r', ({ r, c, s, f }) => {
		setRandom(c, s, f);
		expect(diceRoll(r)).toStrictEqual({ result: f, size: s });
	});

	test('limits dice count to 100000', () => {
		setRandom(100000, 10, []);
		diceRoll('100000d10');
		expect(() => {
			setRandom(100001, 10, []);
			diceRoll('100001d10');
		}).toThrow();
		// @ts-ignore
		dice.mockReset();
	});

	test('limits dice count * size to 100000000000000', () => {
		setRandom(10000, 10000000000, []);
		diceRoll('10000d10000000000');
		expect(() => {
			setRandom(10000, 10000000001, []);
			diceRoll('10000d10000000001');
		}).toThrow();
		// @ts-ignore
		dice.mockReset();
	});
});
