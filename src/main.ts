import {
	MatrixClient,
	SimpleFsStorageProvider,
	AutojoinRoomsMixin,
	RustSdkCryptoStorageProvider,
} from 'matrix-bot-sdk';

import config from './config';
import DiceError from './dice/DiceError';
import roller, { RollAttribute, validateRollString } from './dice/roller';
import getMention from './getMention';

const homeserverUrl = config.homeserver;
const accessToken = config.access_token;
const username = config.username;
const allowedRooms = config.activeRoomIds;

const storage = new SimpleFsStorageProvider('bot_storage.json');
const cryptoProvider = new RustSdkCryptoStorageProvider('crypto_data');

const client = new MatrixClient(
	homeserverUrl,
	accessToken,
	storage,
	cryptoProvider,
);

AutojoinRoomsMixin.setupOnClient(client);

const activeRooms = new Set(allowedRooms);

function handleMention(roomId: string, event: any, commandText: string) {
	if (commandText === 'enable') {
		activeRooms.add(roomId);
		client.replyHtmlText(
			roomId,
			event,
			'Rolling is <b>enabled</b> in this room now!',
		);
	} else if (commandText === 'disable') {
		activeRooms.delete(roomId);
		client.replyHtmlText(
			roomId,
			event,
			'Rolling is <b>disabled</b> in this room now!',
		);
	} else if (commandText === 'status') {
		client.replyHtmlText(
			roomId,
			event,
			`Rolling is <b>${
				activeRooms.has(roomId) ? 'enabled' : 'disabled'
			}</b> in this room`,
		);
	} else {
		client.replyHtmlText(roomId, event, 'Unrecognized command!');
	}
}

client.start().then(() => console.log('Client started!'));

client.on('room.join', (roomId: string, event: any) => {
	console.log(`I've joined room "${roomId}"!`);
});

function formatRollHistory(
	rollHistory: {
		type: RollAttribute;
		rolls: string[];
	}[],
) {
	return rollHistory
		.map(({ rolls, type }) => {
			if (type === 'Fixed' || type == null) {
				return `(${rolls.join(', ')})`;
			}
			return `<b>${type}</b> (${rolls.join(', ')})`;
		})
		.join(', ');
}

type MessageEventCustom = {
	sender: string;
	content: { body: string; formatted_body?: string };
};

const timeoutMap = new Map();
function limiter(sender: string) {
	if (timeoutMap.has(sender)) {
		return false;
	}
	timeoutMap.set(
		sender,
		setTimeout(() => {
			timeoutMap.delete(sender);
		}, 500),
	);
	return true;
}

client.on('room.message', (roomId, event: MessageEventCustom) => {
	try {
		if (!event.content) {
			return;
		}
		if (event.sender === username) {
			return;
		}
		if (!limiter(event.sender)) {
			return;
		}
		const [mentioned, mentionText] = getMention(
			event.content.formatted_body ?? event.content.body,
		);
		if (mentioned) {
			console.log('mentioned!', mentionText);
			handleMention(roomId, event, mentionText);
			return;
		}
		if (!activeRooms.has(roomId)) {
			return;
		}
		const sender = event.sender;
		const body = event.content.body as string;

		console.log(
			`${roomId}: ${sender} says ""${event.content.formatted_body}""`,
		);

		const match = body.match(/^!(roll|r) (.+)$/);
		if (match != null) {
			const roll = match[2] as string;
			const rollError = validateRollString(roll);
			if (typeof rollError === 'string') {
				client.replyHtmlText(
					roomId,
					event,
					`<i>That looks like an invalid roll. ${rollError}</i>`,
				);
			} else {
				try {
					const { value, rollHistory } = roller(roll);
					const historyResult = formatRollHistory(rollHistory);
					const result = `Result: <b>${value}</b> - ${historyResult}`;
					client.replyHtmlText(roomId, event, result);
				} catch (error) {
					if (error instanceof DiceError) {
						client.replyHtmlText(roomId, event, error.message);
					}
				}
			}
			return;
		}
		const inlineMatches = Array.from(body.matchAll(/\{([^}]+)\}/g));
		if (inlineMatches.length > 0) {
			const resultsList = inlineMatches.map(inlineMatch => {
				const roll = inlineMatch[1] as string;
				const rollError = validateRollString(roll);
				if (rollError) {
					return `<i>That looks like an invalid roll. ${rollError}</i>`;
				}
				try {
					const { value, rollHistory } = roller(roll);
					const historyResult = formatRollHistory(rollHistory);
					return `{${roll}}: <b>${value}</b> - ${historyResult}`;
				} catch (error) {
					if (error instanceof DiceError) {
						return `<i>${error.message}</i>`;
					}
					return `<i>Unknown error for ${inlineMatch}</i>`;
				}
			});
			client.replyHtmlText(
				roomId,
				event,
				`Results<ol>${resultsList
					.map(result => {
						return `
<li>${result}</li>`;
					})
					.join('')}</ol>`,
			);
		}
	} catch (error) {
		console.error(error);
	}
});
