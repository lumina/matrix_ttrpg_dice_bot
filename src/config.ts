// @ts-ignore
import validatem from '@validatem/core';
// @ts-ignore
import isString from '@validatem/is-string';
// @ts-ignore
import required from '@validatem/required';
// @ts-ignore
import arrayOf from '@validatem/array-of';
import rawConfig from '../config.json';

const validateValue = validatem.validateValue;

const config = validateValue(rawConfig, {
	homeserver: [required, isString],
	access_token: [required, isString],
	username: [required, isString],
	activeRoomIds: [required, arrayOf([required, isString])],
});

export default config as {
	homeserver: string;
	access_token: string;
	username: string;
	activeRoomIds: string[];
};
