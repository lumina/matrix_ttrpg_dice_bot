import config from './config';

const username = config.username;
const regexEscapedUsername = username.replaceAll('.', '\\.');

export default function getMention(message: string): [boolean, string] {
	const simpleMentionMatch = message.match(
		new RegExp(`^${regexEscapedUsername} (.+)`),
	);
	if (simpleMentionMatch != null) {
		return [true, simpleMentionMatch[1] as string];
	}
	return [false, ''];
}
