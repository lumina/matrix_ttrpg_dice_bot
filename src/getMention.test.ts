import config from './config';
import getMention from './getMention';

const username = config.username;

describe('getMention', () => {
	test('can find a mention', () => {
		expect(getMention('abcd')).toStrictEqual([false, '']);
		expect(getMention(`${username} enable`)).toStrictEqual([true, 'enable']);
	});
});
