Matrix bot for ttrpg-style dice rolls!

Before running the bot, you need to do two things:

Install deps: `npm install`

Create a `config.json` file in the project root. This should be an object with this shape:

```json
{
	"homeserver": "https://your-homeserver.example",
	"access_token": "<access token>",
	"username": "<bot username>",
	"activeRoomIds": [
		"<matrix room ID>"
	]
}
```

Create an account for the bot on your homeserver of choice, optionally setup display name and avatar. Then you can get the access token and username for the config file. ([Instructions for how to get access token in Element client](https://t2bot.io/docs/access_tokens/))

`activeRoomIds` should be an array of the default active roomId values. The bot will log the roomId when joining. In Element, the roomId can be found in room settings under Advanced, as the "Internal room ID". Values will look like `!<random>:<homeserver domain>`. These will be the rooms where the bot is enabled at startup.

Run the server with `npm start`.

For now, the bot is using ts-node, so technically you could get more "performance" by changing the TS setup to compile to a dist folder and then using `node dist/main.js`, but practically it probably doesn't matter much.

## Using the bot

The bot will autojoin any rooms it's invited to. Right now it only support un-encrypted rooms.

### Enabling, disabling, and status

After joining a room, the bot will not do anything unless it is enabled.

You can check the status, enable, and disable the bot using these command sent as message in the room:

```
<bot username> status
<bot username> enable
<bot username> disable
```

For example, if your bot's username is `@myttrpgbot:myhomeserver.example`, then send:

```
@myttrpgbot:myhomeserver.example status
```

Note that you must send the full username in the message, don't let your client replace it with the bot's display name. In element the easiest way to do that is to copy the whole username into the message, instead of typeing it out.

Once the bot is enabled in a room, the roll commands can then be used.

### Direct roll commands

The command format is

```
!r <roll>
```
or
```
!roll <roll>
```

### Inline roll commands

The bot will also recognize rolls that are enclosed in curly braces.

```
I roll {<roll>} and {<roll>}
```

### Dice notation

Dice notation is `<positive integer>d<positive integer>[<mod><positive integer>]`

Examples of dice notation:

- **1d20** -> single 20-sided di roll
- **4d6** -> 4 6-sided dice rolls added together
- **d8** -> single 8-sided di roll
- **10d13** -> number and sides can be arbitrary positive integers
- **2d20k1** or **2d20kh1** -> two 20-sided dice rolled, and **k**eep the **h**ighest one (often called "advantage")
- **3d20kl2** -> 3 20-sided dice, **k**eep the **l**owest two and add the result
- **4d6d1** or **4d6dl1** -> 4 6-sided dice, **d**rop the lowest and add the rest together
- **4d6dh2** -> 4 6-sided dice, **d**rop the two **h**ighest and add the rest together

Dice rolls have a maximum count of 100000, and count * size must be less than 100000000000001.

### Rolls

`<roll>` takes the form of 1 or more dice notation strings or numbers separated by one of the four common math operators with an optional rounding flag. The operators are `+` (addition), `-` (subtration), `*` (multiplication), and `/` (division). All values are rounded down by default at each operation. The operation can be set to instead round up by adding a `u` after the operator. So `/u` or `*u`. You can similarly add a `d` to explicitly round down, but you never need to.

> Note that using an operator with `d` and no space after it will conflict with a no-count roll like `d8`. If you do `20/d20`, it would parse to  `20 /d 20`, not `20 / d20`.

Examples:

- **10** -> a simple numeric value
- **1d20** -> a simple di roll
- **1d20 + 5** -> a di roll plus modifier
- **4d5d1 + 3** -> a complex dice roll plus modifier
- **100 / 1d20** -> 100 divided by a 20-sided dice roll, rounded down
- **3 / 2** -> this would give 1 (division rounded down)
- **3 /u 2** -> this would give 2 (division rounded up)
- **1 + 2 + 3 / 6 - 1** -> can do as many operations as you want. Operations are handled in order, **not** by operator precedence. The result of this operation is `0`, not `2`.
- **3d6/u3    +   3** -> whitespace is ignored between operators and dice or numbers, this parses the same as `3d6 /u 3 + 3`.
- **3 d 6** -> ⚠️ this gives an error. whitespace is *not* ignored within dice roll notation.
- **3 / u 2** -> ⚠️ this gives an error. whitespace is *not* ignored between an operator an rounding modifier.

### Examples

Here's some realistic examples of how you might make rolls

- `!roll 1d20 + 4`
- `!r 4d6d1`
- `Damage and bonus damage {1d8 + 3d6}`
- `!r 2d20k1 - 1`
- `{1d100} for a new location, {1d10} for biome`
- `!r 1d20 + 1d4 + 1d6`
- `!r 2d6 + 8 / 2`
